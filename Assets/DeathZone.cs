using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.GetComponent<Player1Life>())
        {
            other.GetComponent<Player1Life>().LooseHP(10);
            Destroy(gameObject);
        }
    }
}
