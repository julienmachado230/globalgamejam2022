using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RepeaterLight : MonoBehaviour
{
    public GameObject myLight;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Light") && other.gameObject != myLight)
        {
            myLight.transform.DOScale(Vector3.one * 6, 1f);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Light"))
        {
            StartCoroutine(LightCanceled());
        }
    }

    IEnumerator LightCanceled()
    {
        yield return new WaitForSeconds(2);
        myLight.transform.DOScale(Vector3.zero, 2f);
    }
}
