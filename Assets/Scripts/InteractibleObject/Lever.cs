using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    public bool isActivated;
    bool canActivate;
    public TMPro.TMP_Text text;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (canActivate)
        {
            if (player.GetComponent<Player1Movement>())
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    isActivated = !isActivated;
                }
            }
            else if (player.GetComponent<DeplacementJ2>())
            {
                if (Input.GetKeyDown(KeyCode.Joystick1Button1))
                {
                    isActivated = !isActivated;
                }
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canActivate = true;
            player = other.gameObject;

            if (player.GetComponent<Player1Movement>())
            {
                text.text = "E";
            }
            else if (player.GetComponent<DeplacementJ2>())
            {
                text.text = "X/A";
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            text.text = "";
            canActivate = false;
        }
    }
}
