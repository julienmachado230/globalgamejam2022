using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activable : MonoBehaviour
{
    public enum ActiveEffect { Opening, Closing, Falling}
    [SerializeField]
    public ActiveEffect activeEffect;

    public MeshRenderer objectRenderer;
    public Collider objectCollider;
    public Rigidbody objectRigidbody;

    public Lever[] activators;

    // Start is called before the first frame update
    void Start()
    {
        if(GetComponent<MeshRenderer>())
            objectRenderer = GetComponent<MeshRenderer>();
        if (GetComponent<Collider>())
            objectCollider = GetComponent<Collider>();
        if(GetComponent<Rigidbody>()) objectRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isActivated())
        {
            switch (activeEffect)
            {
                case ActiveEffect.Opening:
                    if (objectRenderer != null)
                        objectRenderer.enabled = false;
                    if (objectCollider != null)
                        objectCollider.enabled = false;
                    break;

                case ActiveEffect.Closing:
                    if (objectRenderer != null)
                        objectRenderer.enabled = true;
                    if (objectCollider != null)
                        objectCollider.enabled = true;
                    break;

                case ActiveEffect.Falling:
                    objectRigidbody.useGravity = true;
                    break;

                default:
                    break;

            }
        }
        else
        {
            switch (activeEffect)
            {
                case ActiveEffect.Opening:
                    if (objectRenderer != null)
                        objectRenderer.enabled = true;
                    if (objectCollider != null)
                        objectCollider.enabled = true;
                    break;

                case ActiveEffect.Closing:
                    if (objectRenderer != null)
                        objectRenderer.enabled = false;
                    if (objectCollider != null)
                        objectCollider.enabled = false;
                    break;

                case ActiveEffect.Falling:
                    //objectRigidbody.useGravity = false;
                    break;

                default:
                    break;

            }
        }
    }

    bool isActivated()
    {
        foreach (Lever activator in activators)
        {
            if (!activator.isActivated)
            {
                return false;
            }
        }

        return true;
    }
}
