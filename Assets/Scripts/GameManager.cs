using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public GameObject creditPanel;
    public GameObject howToPlayPanel;

    // Start is called before the first frame update
    void Start()
    {
        creditPanel.SetActive(false);
        howToPlayPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Play()
    {
        howToPlayPanel.SetActive(true);
    }

    public void Credit()
    {
        creditPanel.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Return()
    {
        creditPanel.SetActive(false);
    }

    public void Continue()
    {
        SceneManager.LoadScene("Test Ld");
    }
}
