using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player1Life : MonoBehaviour
{
    public int HP;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.H)) LooseHP(1);
    }

    public void LooseHP(int value)
    {
        if(HP - value > 0)
        {
            HP -= value;
        }
        else
        {
            SceneManager.LoadScene("Test Ld");
        }
    }
}
