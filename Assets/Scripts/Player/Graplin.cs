using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Graplin : MonoBehaviour
{
    public Transform playerLightTransform;
    public Transform graplinLightTransform;
    public Transform playerTransform;
    public Transform graplinTransform;

    public Player1Movement player1movement;

    public float maxDistance;

    public float fullScaleLightRation;
    public float halfScaleLightRation;

    Vector3 endPosition;
    Vector3 startPosition;

    float currentPos;

    bool isLightThrown;
    bool isTargeting;
    bool canThrow;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Targeting();
        CheckWall();
        ThrowingLight();
        if (isLightThrown) MoveLight();
    }

    void Targeting()
    {
        if (Input.GetMouseButtonDown(1) && !isLightThrown)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            player1movement.canMove = false;

            graplinLightTransform.gameObject.SetActive(true);

            //StartCoroutine(GoToScale(3, Vector3.one * 5, playerLightTransform));

            playerLightTransform.DOScale(Vector3.one * halfScaleLightRation, 0.5f);

            isTargeting = true;
        }

        if (Input.GetMouseButtonUp(1))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            player1movement.canMove = true;

            graplinLightTransform.position = playerTransform.position;
            StartCoroutine(deactivateLight());
            //graplinLightTransform.gameObject.SetActive(false);

            playerLightTransform.DOScale(Vector3.one * fullScaleLightRation, 0.5f);


            currentPos = 0;

            isTargeting = false;
            isLightThrown = false;

            graplinTransform.eulerAngles = Vector3.zero;
            graplinTransform.localScale = Vector3.one;
            graplinTransform.gameObject.SetActive(false);

        }
    }

    void CheckWall()
    {
        Ray ray;
        RaycastHit hit;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag("Ground"))
            {
                canThrow = true;
                Debug.Log("CAN THROW");
            }
            else
            {
                canThrow = false;
            }
        }
        else
        {
            canThrow = false;
        }
    }

    void ThrowingLight()
    {
        if (Input.GetMouseButtonDown(0) && isTargeting && !isLightThrown /*&& canThrow*/)
        {
            var mousePos = Input.mousePosition;
            mousePos.z = 10.0f;
            endPosition = Camera.main.ScreenToWorldPoint(mousePos);

            startPosition = graplinLightTransform.position;

            Vector3 targetVector = (endPosition - startPosition).normalized;
            targetVector = new Vector3(targetVector.x, targetVector.y, 0);
            RaycastHit hit;

            int layerMask = LayerMask.GetMask("Ground");

            if (Physics.Raycast(transform.position, targetVector, out hit, maxDistance, layerMask))
            {
                Debug.DrawRay(transform.position, targetVector * 1000, Color.red, 10);
                Debug.Log("Did Hit : " + hit.transform.gameObject.name);
                endPosition = hit.point;

                if (hit.transform.tag == "Ground")
                {
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;

                    isTargeting = false;
                    isLightThrown = true;

                    graplinTransform.gameObject.SetActive(true);

                    float angle = Mathf.Atan2(endPosition.y - startPosition.y, endPosition.x - startPosition.x) * 180 / Mathf.PI;
                    float distance = Vector3.Distance(startPosition, endPosition) / 2;

                    graplinTransform.eulerAngles = new Vector3(0, 0, angle);
                    //StartCoroutine(GoToScale(2, new Vector3(distance, graplinTransform.localScale.y, graplinTransform.localScale.z), graplinTransform));
                    graplinTransform.DOScale(new Vector3(distance, graplinTransform.localScale.y, graplinTransform.localScale.z), 0.1f);
                }
            }
        }
    }

    void MoveLight()
    {
        currentPos += Input.GetAxis("Mouse ScrollWheel") * 0.5f;

        graplinLightTransform.position = Vector3.Lerp(startPosition, endPosition, currentPos);
    }

    IEnumerator deactivateLight()
    {
        MeshRenderer lightRenderer = graplinLightTransform.gameObject.GetComponent<MeshRenderer>();
        lightRenderer.enabled = false;
        yield return new WaitForSeconds(0.1f);
        graplinLightTransform.gameObject.SetActive(false);
        lightRenderer.enabled = true;

    }
}
