using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Movement : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    [HideInInspector]
    public bool canMove;

    public Transform graplinStart;

    bool canJump;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, 0);

        if (canMove)
        {
            Move();
            Jump();
        }
    }

    public void Move()
    {
        float horizontal = Input.GetAxis("Horizontal2");

        gameObject.transform.position += new Vector3(horizontal, 0, 0) * Time.deltaTime * speed;

        if(horizontal > 0)
        {
            gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
            graplinStart.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (horizontal < 0)
        {
            gameObject.transform.localEulerAngles = new Vector3(0, 180, 0);
            graplinStart.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    public void Jump()
    {
        if(canJump && Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("JUMP");

            gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("ENTER");
        if (other.CompareTag("Ground"))
        {
            Debug.Log("ENTER GROUND");
            canJump = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            canJump = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            Debug.Log("EXIT GROUND");
            canJump = false;
        }
    }
}
