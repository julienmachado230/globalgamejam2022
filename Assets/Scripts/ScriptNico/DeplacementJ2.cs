using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeplacementJ2 : MonoBehaviour
{
    private float vertical;
    private float horizontal;

    [SerializeField] private GameObject Joueur2;

    [SerializeField] private Rigidbody rb_Joueur2;

    public Transform player1;

    public bool isAttack;
    public bool IsInLight;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        Joueur2 = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");

        rb_Joueur2 = Joueur2.GetComponent<Rigidbody>();

        //Permet le d�placement du joueur 2
        rb_Joueur2.AddForce(transform.up * vertical * 2.8f);

        if (horizontal > 0)
        {
            gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
            rb_Joueur2.AddForce(transform.right * horizontal * 2.8f);
        }
        else if (horizontal < 0)
        {
            gameObject.transform.localEulerAngles = new Vector3(0, 180, 0);
            rb_Joueur2.AddForce(transform.right * -horizontal * 2.8f);
        }

        //Si les input ne sont pkus utilis�, stop la v�locit� de fa�on d�gressif
        if (-0.5f < vertical || vertical < 0.5f && -0.5f < horizontal || horizontal < 0.5f)
        {
            rb_Joueur2.velocity = new Vector3(rb_Joueur2.velocity.x / 1.01f, rb_Joueur2.velocity.y / 1.01f, rb_Joueur2.velocity.z / 1.01f);
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button3) && isAttack == false)
        {
            StartCoroutine(Attack());
        }
    }


    IEnumerator Attack()
    {
        rb_Joueur2.AddForce(rb_Joueur2.velocity * 2.4f, ForceMode.Impulse);

        isAttack = true;

        yield return new WaitForSeconds(1);

        isAttack = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MainLight") || other.CompareTag("Light"))
        {
            IsInLight = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("MainLight") || other.CompareTag("Light"))
        {
            IsInLight = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainLight") || other.CompareTag("Light"))
        {
            IsInLight = false;
            StartCoroutine(BackToPlayer());
            Debug.Log("OUT");
        }
    }

    IEnumerator BackToPlayer()
    {
        Debug.Log("LAUNCH OUT");
        yield return new WaitForSeconds(1f);
        if (!IsInLight)
        {
            transform.position = new Vector3(player1.position.x, player1.position.y + 1, 0);
        }
    }
}
