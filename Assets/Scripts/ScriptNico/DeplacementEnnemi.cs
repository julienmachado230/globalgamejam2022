using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeplacementEnnemi : MonoBehaviour
{

    public GameObject pointA;
    public GameObject pointB;
    public GameObject position;

    public float speed;

    public bool goToPointA = true;
    public bool goToPointB = false;
    public bool playerDetected;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (goToPointA)
        {

            transform.position = Vector3.MoveTowards(transform.position, pointA.transform.position, speed);

            transform.eulerAngles = new Vector3(transform.rotation.x, 180, transform.rotation.z);

        }
        else if (goToPointB)
        {

            transform.position = Vector3.MoveTowards(transform.position, pointB.transform.position, speed);

            transform.eulerAngles = new Vector3(transform.rotation.x, 0, transform.rotation.z);

        }
        if (playerDetected)
        {
            Vector3 lookPlayer = GetComponentInChildren<DetectionPlayer>().player.transform.position - transform.position;
            if (lookPlayer.x > 0)
            {
                transform.eulerAngles = new Vector3(transform.rotation.x, 0, transform.rotation.z);

            }
            else if (lookPlayer.x < 0)
            {
                transform.eulerAngles = new Vector3(transform.rotation.x, 180, transform.rotation.z);

            }
            transform.position = Vector3.MoveTowards(transform.position, GetComponentInChildren<DetectionPlayer>().player.transform.position, speed);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == pointA.GetComponent<Collider>() && !playerDetected)
        {
            goToPointB = true;
            goToPointA = false;
        }
        if (other == pointB.GetComponent<Collider>() && !playerDetected)
        {
            goToPointB = false;
            goToPointA = true;
        }

        if (other.CompareTag("Player") && other.GetComponent<Player1Life>())
        {
            other.GetComponent<Player1Life>().LooseHP(10);
            Destroy(gameObject);
        }
        else if(other.CompareTag("Player") && other.GetComponent<DeplacementJ2>() && other.GetComponent<DeplacementJ2>().isAttack)
        {
            Destroy(gameObject);
        }
    }
}
