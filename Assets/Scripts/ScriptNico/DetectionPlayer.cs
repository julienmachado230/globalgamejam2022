using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionPlayer : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && other.GetComponent<Player1Life>())
        {
            transform.parent.GetComponent<DeplacementEnnemi>().playerDetected = true;
            transform.parent.GetComponent<DeplacementEnnemi>().goToPointA = false;
            transform.parent.GetComponent<DeplacementEnnemi>().goToPointB = false;

            player = other.gameObject;

        }
    }
}
